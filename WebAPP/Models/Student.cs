﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using MongoDB.Driver;

namespace WebAPP.Models
{
    public class Student
    {
        [BsonId]
        public int _id { get; set; }
        public string name { get; set; }
        public int age { get; set; }
        public string gender { get; set; } 
        public int cls { get; set; }
    }
}
