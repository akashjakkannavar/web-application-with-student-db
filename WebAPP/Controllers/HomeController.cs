﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPP.Models;
using MongoDB.Bson;
using MongoDB.Driver;


namespace WebAPP.Controllers
{
    public class HomeController : Controller
    {
        private IMongoDatabase mongoDatabase;

        
        public IMongoDatabase GetMongoDatabase()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");
            return mongoClient.GetDatabase("SchoolDb");
        }

        [HttpGet]
        public IActionResult Index()
        {
            
            mongoDatabase = GetMongoDatabase();
            
            var result = mongoDatabase.GetCollection<Student>("Student").Find(s=>true).ToList();
            var result2 = result.OrderBy(s => s._id).ToList();
            return View(result2);
        }

        [HttpGet]
        public IActionResult Sort()
        {
            mongoDatabase = GetMongoDatabase();

            var student = mongoDatabase.GetCollection<Student>("Student").Find<Student>(k => true).ToList();
            if (student == null)
            {
                return NotFound();
            }
            var student2 = student.OrderBy(s => s.name).ToList();
            return View(student2);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            try
            {
                
                mongoDatabase = GetMongoDatabase();
                mongoDatabase.GetCollection<Student>("Student").InsertOne(student);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            mongoDatabase = GetMongoDatabase();
            
            Student student = mongoDatabase.GetCollection<Student>("Student").Find<Student>(k => k._id == id).FirstOrDefault();
            if (student== null)
            {
                return NotFound();
            }
            return View(student);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            mongoDatabase = GetMongoDatabase();
            Student student = mongoDatabase.GetCollection<Student>("Student").Find<Student>(k => k._id== id).FirstOrDefault();
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        [HttpPost]
        public IActionResult Delete(Student student)
        {
            try
            {
                //Get the database connection
                mongoDatabase = GetMongoDatabase();
                //Delete the customer record
                var result = mongoDatabase.GetCollection<Student>("Student").DeleteOne<Student>(k => k._id == student._id);
                if (result.IsAcknowledged == false)
                {
                    return BadRequest("Unable to remove student " + student._id);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            mongoDatabase = GetMongoDatabase();
            var student = mongoDatabase.GetCollection<Student>("Student").Find<Student>(k => k._id == id).FirstOrDefault();
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            try
            {
                
                mongoDatabase = GetMongoDatabase();
                
                var filter = Builders<Student>.Filter.Eq("_id", student._id);
                
                var updatestatement = Builders<Student>.Update.Set("_id", student._id);
                updatestatement = updatestatement.Set("name", student.name);
                updatestatement = updatestatement.Set("gender", student.gender);
                
                var result = mongoDatabase.GetCollection<Student>("Student").UpdateOne(filter, updatestatement);
                if (result.IsAcknowledged == false)
                {
                    return BadRequest("Unable to update Student  " + student.name);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RedirectToAction("Index");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }




}
